import os, json
import traceback
import boto3
from botocore.exceptions import ClientError

def get_secret_manager():
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=os.environ['REGION'],
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=os.environ['SECRET_NAME']
        )
    except ClientError as e:
        raise Exception(e)
    else:
        secret = get_secret_value_response['SecretString']
        return json.loads(secret)


class DDB_Action(object):
    def __init__(self):
        secret_param = get_secret_manager()
        dynamodb = boto3.resource(
            'dynamodb',
            region_name=os.environ['REGION'],
            aws_access_key_id=secret_param['aws_access_key_id'],
            aws_secret_access_key=secret_param['aws_secret_access_key']
        )

        self.table = dynamodb.Table('notihub')

    def is_exist(self, primary_key):
        response = self.table.get_item(
            Key=primary_key
        )

        item = response.get('Item', 0)

        if item:
            return True

        return False

    def delete(self, event):
        primary_key = {
            'account_id': event['account_id'],
            'alarm_name': event['alarm_name']
        }

        if not self.is_exist(primary_key):
            raise Exception('This item is not exist')

        response = self.table.delete_item(
            Key=primary_key
        )


def main(event, context):
    try:
        event = json.loads(event['body'])
        ddb = DDB_Action()
        ddb.delete(event)

        status_code = 200
        body = 'delete item success.'

    except ClientError as e:
        status_code = 500
        body = e.response['Error']['Message']
        print(traceback.format_exc())
        
    except Exception as e:
        status_code = 400
        body = e
        print(traceback.format_exc())
    
    return {
        'statusCode': status_code,
        'body': json.dumps(body),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        }
    }