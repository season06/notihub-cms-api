provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region     = var.region
}

module "api_gateway" {
  source = "terraform-aws-modules/apigateway-v2/aws"

  name          = "notihub-cms-api"
  description   = "Notihub CMS API"
  protocol_type = "HTTP"

  cors_configuration = {
    allow_headers = ["*"]
    allow_methods = ["*"]
    allow_origins = ["*"]
  }

  create_api_domain_name = false
  create_vpc_link        = false

  # Routes and integrations
  integrations = {
    "GET /notihub_get_all" = {
      lambda_arn             = "arn:aws:lambda:${var.region}:${var.account_id}:function:cms_notihub_get_all"
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }
    "POST /notihub_create" = {
      lambda_arn             = "arn:aws:lambda:${var.region}:${var.account_id}:function:cms_notihub_create"
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }
    "POST /notihub_update" = {
      lambda_arn             = "arn:aws:lambda:${var.region}:${var.account_id}:function:cms_notihub_update"
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }
    "POST /notihub_delete" = {
      lambda_arn             = "arn:aws:lambda:${var.region}:${var.account_id}:function:cms_notihub_delete"
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }
    "GET /prtg_get_all" = {
      lambda_arn             = "arn:aws:lambda:${var.region}:${var.account_id}:function:cms_prtg_get_all"
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }
    "POST /prtg_delete" = {
      lambda_arn             = "arn:aws:lambda:${var.region}:${var.account_id}:function:cms_prtg_delete"
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }
  }

  depends_on = [module.lambda]

}

# Lambda
module "lambda" {
  for_each = toset(var.lambda_function)

  source = "terraform-aws-modules/lambda/aws"

  create_current_version_allowed_triggers = false
  create_role                             = false

  function_name = "cms_${each.key}"
  runtime       = "python3.8"
  memory_size   = "128"
  timeout       = "10"
  handler       = "${each.key}.main"
  source_path   = "./src/${each.key}.py"

  environment_variables = {
    REGION      = var.region
    SECRET_NAME = var.secret_name
  }

  lambda_role = aws_iam_role.lambda_role.arn
}

resource "aws_lambda_permission" "apigw_lambda" {
  for_each = toset(var.lambda_function)

  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "cms_${each.key}"
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${module.api_gateway.apigatewayv2_api_execution_arn}/*/*/${each.key}"

  depends_on = [module.lambda, module.api_gateway]
}

# iam role
resource "aws_iam_role" "lambda_role" {
  name               = "notihub_cms_lambda_role"
  assume_role_policy = file("./iam_policy/lambda_role.json")
}

resource "aws_iam_policy" "lambda_policy" {
  name   = "notihub_cms_lambda_policy"
  policy = file("./iam_policy/lambda_policy.json")
}

resource "aws_iam_policy_attachment" "lambda_attach" {
  name       = "lambda_attachment"
  roles      = [aws_iam_role.lambda_role.name]
  policy_arn = aws_iam_policy.lambda_policy.arn
}
